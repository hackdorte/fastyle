# Demo Page
### A demonstration of Fastyle in action.

**Q: Can I use the page for my project?**

**A**: Yes!

**Q: Is there any license?**

**A**: No! This is dedicated to the public domain.